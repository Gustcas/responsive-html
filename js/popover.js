$(document).ready(function () {
  $(".bnt-popover").popover({
    html: true,
    placement: "bottom",
    content: function () {
      return "Info. Piano di viaggio";
    },
  });

  $(".bnt-popover").mouseover(() => {
    $(".bnt-popover").popover("show");
  });
  $(".bnt-popover").mouseleave(() => {
    $(".bnt-popover").popover("hide");
  });
});
